package table

import (
  "testing"
  "math/rand"
  "fmt"
)

func TestIntArrayExists(t *testing.T) {
  array := NewIntArray()
  var n uint32
  for i := uint32(0); i < size_65k; i += 1 {
    n = rand.Uint32()
    array.IntArrayAppendSingle(n)
  }
  found := array.IntArrayExists(n)
  Assert(t, found, fmt.Sprintf("Should be in array %d", n))
}
