package table

import (
	"bytes"
  "os"
  "encoding/csv"
  "io"
  "fmt"
  "strconv"
)

const size_0b uint32 = 0
const size_16b uint32 = 16
const size_512b uint32 = 512
const size_65k uint32 = 65536
const size_4mb uint32 = 4194304
const size_1gb uint32 = 1073741824

type ByteArrayInterface interface {
	ByteArrayAppend(bytes []byte)
	ByteArrayAppendSingle(b byte)
	ByteArraySize() uint32
	ByteArrayExists(key []byte) bool
	ByteArrayEquals(key []byte) bool
  ByteArrayAt(position uint32) byte
  ByteArrayAppendCompact(bytes uint32)
  ByteArrayIterateCompact(f func(uint32, uint32)) bool
  ByteArrayIterateAllCompact(bytes []uint32) bool
  ByteArrayExistsCompact(key uint32) bool
  ByteArrayFirstCompact() uint32
  ByteArrayPopCompact()
  ByteArrayMakeEmpty()
}

type ByteArray struct {
	bytes []byte
}

func NewByteArray(bytes ...[]byte) *ByteArray {
	// creates new byte array
	array := &ByteArray{}
	if len(bytes) > 0 {
		array.ByteArrayAppend(bytes[0])
	}
	return array
}

func (this *ByteArray) ByteArrayAppend(bytes []byte) {
	// appends to byte array
	this.bytes = append(this.bytes, bytes...)
}

func (this *ByteArray) ByteArrayAppendSingle(b byte) {
	// appends single character to byte array
	this.bytes = append(this.bytes, b)
}

func (this *ByteArray) ByteArrayAt(position uint32) byte {
  // get byte at position
  return this.bytes[position]
}

func (this *ByteArray) ByteArraySize() uint32 {
	// gets byte array size
	return uint32(len(this.bytes))
}

func (this *ByteArray) ByteArrayExists(key []byte) bool {
	// checks if key exists in byte array
	return bytes.Contains(this.bytes, key)
}

func (this *ByteArray) ByteArrayEquals(key []byte) bool {
	// checks if two byte slices are equal
	return bytes.Equal(this.bytes, key)
}

func (this *ByteArray) ByteArrayAppendCompact(bytes uint32) {
  // append bytes in variable length quantity format
  // used for compact ints
  // do not mix with append
  // http://en.wikipedia.org/wiki/Variable-length_quantity
  mask := byte(0x7f)
  lastBit := byte(0x80)
  first := byte(bytes) & mask
  second := byte(bytes>>7) & mask
  third := byte(bytes>>14) & mask
  fourth := byte(bytes>>21) & mask
  fifth := byte(bytes>>28) & mask
  blocks := uint32(0)
  if fifth > 0 {
    this.ByteArrayAppendSingle(fifth | lastBit)
    blocks += 1
  }
  if fourth > 0 || blocks > 0 {
    this.ByteArrayAppendSingle(fourth | lastBit)
    blocks += 1
  }
  if third > 0 || blocks > 0 {
    this.ByteArrayAppendSingle(third | lastBit)
    blocks += 1
  }
  if second > 0 || blocks > 0 {
    this.ByteArrayAppendSingle(second | lastBit)
    blocks += 1
  }
  if first > 0 || blocks > 0 {
    this.ByteArrayAppendSingle(first)
  }
}

func (this *ByteArray) ByteArrayIterateCompact(f func(uint32, uint32) bool) bool {
  // iterate through compact ints
  // todo this in parallel
  size := this.ByteArraySize()
  mask := byte(0x7f)
  lastBit := byte(0x80)
  bytes := uint32(0)
  shift := uint32(0)
  for i := uint32(0); i < size; {
    bytes = 0
    shift = 0
    for true {
      bytes <<= shift
      shift = 7
      position := i
      at := this.ByteArrayAt(position)
      i += 1
      if (at & lastBit) > 0 {
        bytes |= uint32(at & mask)
      } else {
        bytes |= uint32(at)
        break
      }
    }
    if(f(i, bytes) == true) {
      return true
    }
  }
  return false
}

func (this *ByteArray) ByteArrayIterateAllCompact(bytes []uint32) bool {
  // used to check condition for all elements
  // todo this in parallel
  for i := 0; i < len(bytes); i += 1 {
    there := this.ByteArrayIterateCompact(func (ii uint32, item uint32) bool {
      return item == bytes[i]
    })
    if(there == false) {
      return false
    }
  }
  return true
}

func (this *ByteArray) ByteArrayExistsCompact(key uint32) bool {
  // check if element exists in compact array
  // todo this in parallel
  if this.ByteArraySize() <= 0 {
    return false
  }
  return this.ByteArrayIterateCompact(func (ii uint32, item uint32) bool {
    return item == key
  })
}

func (this *ByteArray) ByteArrayFirstCompact() uint32 {
  // get first element from compact array
  var key uint32
  this.ByteArrayIterateCompact(func (ii uint32, item uint32) bool {
    key = item
    return true
  })
  return key
}

func (this *ByteArray) ByteArrayPopCompact() {
  // remove first element from compact array
  var i uint32
  this.ByteArrayIterateCompact(func (ii uint32, item uint32) bool {
    i = ii
    return true
  })
  this.bytes = this.bytes[i:]
}

func (this *ByteArray) ByteArrayMakeEmpty() {
  // make array empty
  this.bytes = []byte{}
}

type IntArrayInterface interface {
	IntArrayAppendSingle(b uint32)
	IntArraySize() uint32
	IntArrayExists(key uint32) bool
  IntArrayAt(position uint32) uint32
  IntArrayIterate(bytes []uint32) bool
  IntArrayMakeEmpty()
}

type IntArray struct {
  bytes []uint32
}

func NewIntArray() *IntArray {
  return &IntArray{}
}

func (this *IntArray) IntArrayAppendSingle(b uint32) {
  this.bytes = append(this.bytes, b)
}

func (this *IntArray) IntArraySize() uint32 {
  return uint32(len(this.bytes))
}

func (this *IntArray) IntArrayExists(key uint32) bool {
  for i := 0; i < len(this.bytes); i += 1 {
    if this.bytes[i] == key {
      return true
    }
  }
  return false
}

func Prepend(to []byte, str string) []byte {
	// prepends to byte slice
	return append([]byte(str), to...)
}

func Append(to []byte, str string) []byte {
	// appends to byte slice
	return append(to, []byte(str)...)
}

type Entry struct {
  id uint32
  property *ByteArray
  // todo make this array of properties
  // todo maybe not just hash properties as new nodes
}

func NewEntry(propertyValue ...[]byte) *Entry {
  // create new entry
  entry := &Entry{id: 0, property: NewByteArray()}
  if len(propertyValue) > 0 {
    entry.property.ByteArrayAppend(propertyValue[0])
  }
  return entry
}

func Djb2(key []byte, tableSize uint32) uint32 {
	// djb2 hash function
	var hash uint32 = 5381
	for i := 0; i < len(key); i++ {
		hash = hash<<5 + hash + uint32(key[i])
	}
	return hash % tableSize
}

func ProbeLinear(position uint32, offset uint32, tableSize uint32) uint32 {
	// linear probe for position in table
	return (position + offset) % tableSize
}

type TableArrayAny struct {
	entries []*Entry
  edges []*ByteArray
	size float32
	capacity float32
	loadFactor float32
	maxTries uint32
}

func NewTableArrayAny(params ...float32) *TableArrayAny {
  // creates new table
  var capacity float32 = float32(size_4mb)
  if len(params) > 0 {
    capacity = params[0]
  }
	return &TableArrayAny{make([]*Entry, uint32(capacity)), make([]*ByteArray, uint32(capacity)), 0, capacity, 0.5, size_65k}
}

func (this *TableArrayAny) TableInsertLinear(entry *Entry) {
	// insert with linear probe
  if this.size / this.capacity > this.loadFactor {
    this.tableResize()
  }
  this.tableInsertLinear(entry)
}

func (this *TableArrayAny) tableResize() {
	// resize table if load factor reached
  oldEntries, oldEdges := this.entries, this.edges
  this.capacity = this.capacity * 2 + this.size
  this.entries = make([]*Entry, uint32(this.capacity))
  this.edges = make([]*ByteArray, uint32(this.capacity))
  for i := 0; i < len(oldEntries); i += 1 {
    if oldEntries[i] == nil {
      continue
    }
    this.entries[i], this.edges[i] = oldEntries[i], oldEdges[i]
  }
}

func (this *TableArrayAny) tableInsertLinear(entry *Entry) {
	// actual insert into table with linear probe
  // todo see about insert duplicates
  position := Djb2(entry.property.bytes, uint32(this.capacity))
  test := this.entries[position]
  if test == nil {
    entry.id = position
    this.entries[position] = entry
    this.size += 1
    return
  }
  for i := uint32(0); i < this.maxTries; i++ {
    anotherPosition := ProbeLinear(position, i, uint32(this.maxTries))
    test := this.entries[anotherPosition]
    if test == nil {
      entry.id = anotherPosition
      this.entries[anotherPosition] = entry
      this.size += 1
      return
    }
  }
}

func (this *TableArrayAny) TableInsertId(entry *Entry) {
	// insert with existing id
  if this.size / this.capacity > this.loadFactor {
    this.tableResize()
  }
  this.tableInsertId(entry)
}

func (this *TableArrayAny) tableInsertId(entry *Entry) {
	// insert with existing id
  test := this.entries[entry.id]
  if test == nil {
    this.entries[entry.id] = entry
    this.size += 1
  }
}

func (this *TableArrayAny) TableFind(id uint32) *Entry {
  // find node by id
  return this.entries[id]
}

func (this *TableArrayAny) TableExists(id uint32) bool {
  // check if entry in table
  test := this.entries[id]
  if test != nil {
    if test.id == id {
      return true
    }
  }
  return false
}

func (this *TableArrayAny) TableRemove(id uint32) {
  // remove entry from table
  this.entries[id] = nil
  this.edges[id] = nil
  this.size -= 1
}

func (this *TableArrayAny) TableNeighbours(id uint32) (uint32, []*Entry) {
  // find node's neighbours
  entries := []*Entry{}
  test := this.entries[id]
  edges := this.edges[id]
  if test != nil && edges != nil {
    edges.ByteArrayIterateCompact(func (ii uint32, item uint32) bool {
      entry := this.entries[item]
      if entry == nil {
        return false
      }
      entries = append(entries, entry)
      return false
    })
  }
  return uint32(len(entries)), entries
}

func (this *TableArrayAny) TableCreateEdgeLinear(entryFrom *Entry, entryTo *Entry) {
  // creates edge between nodes
  // if nodes do not exist they will be created
  // todo see about insert duplicates
  this.TableInsertLinear(entryFrom)
  this.TableInsertLinear(entryTo)
  if this.edges[entryFrom.id] == nil {
    this.edges[entryFrom.id] = NewByteArray()
  }
  this.edges[entryFrom.id].ByteArrayAppendCompact(entryTo.id)
}

func (this *TableArrayAny) TableEstablishEdge(idFrom uint32, idTo uint32) {
  // establish edge between existing nodes
  if this.edges[idFrom] == nil {
    this.edges[idFrom] = NewByteArray()
  }
  this.edges[idFrom].ByteArrayAppendCompact(idTo)
}

func (this *TableArrayAny) TableExistsEdge(idFrom uint32, idTo uint32) bool {
  // check if edge exists
  from := this.edges[idFrom]
  if from != nil {
    return from.ByteArrayExistsCompact(idTo)
  }
  return false
}

func (this *TableArrayAny) TableSearch(id uint32) (float32, float32) {
  // search table breadth first
  size, neighbours := this.TableNeighbours(id)
  depth := uint32(0)
  paths := float32(0)
  lengths := float32(0)
  if size <= 0 {
    return float32(depth), paths
  }
  this.TableRemove(id)
  queue := NewQueue(neighbours)
  depth = 1
  for queue.QueueSize() > 0 {
    entry := queue.QueueGet()
    queue.QueuePop()
    size -= 1
    queue.QueuePushRange(this.TableNeighbours(entry))
    this.TableRemove(entry)
    paths += 1
    lengths += float32(depth)
    if size == 0 {
      size = queue.QueueSize()
      depth += 1
    }
  }
  return float32(depth), lengths / paths
}

type QueueInterface interface {
  QueuePush(entry *Entry)
  QueuePushRange(size uint32, entries []*Entry)
  QueueGet() uint32
  QueuePop()
  QueueSize() uint32
}

type Queue struct {
  entries *ByteArray
  size uint32
}

func NewQueue(entries ...[]*Entry) *Queue {
  // make a queue
  queue := &Queue{}
  if len(entries) > 0 {
    queue.QueuePushRange(uint32(len(entries[0])), entries[0])
  }
  return queue
}

func (this *Queue) QueuePush(entry *Entry) {
  // push single element to queue
  if this.entries == nil {
    this.entries = NewByteArray()
  }
  this.entries.ByteArrayAppendCompact(entry.id)
  this.size += 1
}

func (this *Queue) QueuePushRange(size uint32, entries []*Entry) {
  // push multiple element to queue
  if size <= 0 {
    return
  }
  for _, entry := range entries {
    this.QueuePush(entry)
  }
}

func (this *Queue) QueueGet() uint32 {
  // get element from queue
  return this.entries.ByteArrayFirstCompact()
}

func (this *Queue) QueuePop() {
  // pop element from queue
  this.entries.ByteArrayPopCompact()
  this.size -= 1
}

func (this *Queue) QueueSize() uint32 {
  // get queue size
  return this.size
}

func TableImportDiskIdsOnly(insert func(entry *Entry), establishEdge func(idFrom uint32, idTo uint32), file string) {
  // import key value pairs from disk into table in tsv format
  filePointer, err := os.Open(file)
  if err != nil {
    fmt.Fprintf(os.Stderr, "Error %s reading file %s", err.Error(), file)
    return
  }
  defer filePointer.Close()
  // close on exit
  reader := csv.NewReader(filePointer)
  reader.Comma, reader.FieldsPerRecord = '\t', 2
  if err != nil {
    fmt.Fprintf(os.Stderr, "Error %s reading file %s", err.Error(), file)
    return
  }
  for {
    line, err := reader.Read()
    if err == io.EOF {
      break
    } else if err != nil {
      fmt.Fprintf(os.Stderr, "Error %s reading file %s", err.Error(), file)
      return
    }
    entryFrom := NewEntry()
    n, _ := strconv.Atoi(line[0])
    entryFrom.id = uint32(n)
    insert(entryFrom)
    entryTo := NewEntry()
    n, _ = strconv.Atoi(line[1])
    entryTo.id = uint32(n)
    insert(entryTo)
    establishEdge(entryFrom.id, entryTo.id)
  }
}
