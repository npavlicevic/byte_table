# Byte table
Compact graph memory store built on hash table
## Example
See tests for examples
## Tests
sh get_skitter.sh (gets skitter data set from [snap](http://snap.stanford.edu/data/as-skitter.html))  
go test
