package table

import (
	"testing"
  "math/rand"
  "fmt"
)

func TableArrayAnyFind(t *testing.T) {
  table := NewTableArrayAny()
  str := make([]byte, size_16b/2)
  var entry *Entry
  var found *Entry
  for i := uint32(0); i < size_65k; i += 1 {
    RandomString(str)
    entry = NewEntry(str)
    table.TableInsertLinear(entry)
  }
  found = table.TableFind(entry.id)
  Assert(t, found.id == entry.id, "Entry should be in table")
}

func TestTableArrayAnyExistsLinear(t *testing.T) {
  table := NewTableArrayAny()
  str := make([]byte, size_16b/2)
  var entry *Entry
  for i := uint32(0); i < size_65k; i += 1 {
    RandomString(str)
    entry = NewEntry(str)
    table.TableInsertLinear(entry)
  }
  Assert(t, table.TableExists(entry.id), "Entry should be in table")
}

func TestTableArrayAnyExistsId(t *testing.T) {
  table := NewTableArrayAny()
  var entry *Entry
  for i := uint32(0); i < size_65k; i += 1 {
    entry = NewEntry()
    entry.id = rand.Uint32() % uint32(table.capacity)
    table.TableInsertId(entry)
  }
  Assert(t, table.TableExists(entry.id), "Entry should be in table")
}

func TestTableArrayAnyCreateEdgeLinear(t *testing.T) {
  strFrom := make([]byte, size_16b/2)
  var entryFrom *Entry
  strTo := make([]byte, size_16b/2)
  var entryTo *Entry
  table := NewTableArrayAny()
  for i := uint32(0); i < size_65k; i += 1 {
    RandomString(strFrom)
    entryFrom = NewEntry(strFrom)
    RandomString(strTo)
    entryTo = NewEntry(strTo)
    table.TableCreateEdgeLinear(entryFrom, entryTo)
  }
  Assert(t, table.TableExistsEdge(entryFrom.id, entryTo.id), "Edge should exist")
}

func TestTableArrayAnyEstablishEdge(t *testing.T) {
  var entryFrom *Entry
  var entryTo *Entry
  table := NewTableArrayAny()
  for i := uint32(0); i < size_65k; i += 1 {
    entryFrom = NewEntry()
    entryFrom.id = rand.Uint32() % uint32(table.capacity)
    table.TableInsertId(entryFrom)
    entryTo = NewEntry()
    entryTo.id = rand.Uint32() % uint32(table.capacity)
    table.TableInsertId(entryTo)
    table.TableEstablishEdge(entryFrom.id, entryTo.id)
  }
  Assert(t, table.TableExistsEdge(entryFrom.id, entryTo.id), "Edge should exist")
}

func TestTableArrayAnyNeighbours(t *testing.T) {
  var entryFrom *Entry
  var entryTo *Entry
  table := NewTableArrayAny()
  for i := uint32(0); i < size_65k; i += 1 {
    entryFrom = NewEntry()
    entryFrom.id = rand.Uint32() % uint32(table.capacity)
    table.TableInsertId(entryFrom)
    entryTo = NewEntry()
    entryTo.id = rand.Uint32() % uint32(table.capacity)
    table.TableInsertId(entryTo)
    table.TableEstablishEdge(entryFrom.id, entryTo.id)
  }
  n, neighbours := table.TableNeighbours(entryFrom.id)
  Assert(t, n > 0 && neighbours[0].id > 0, "There should be neighbours")
}

func TestTableArrayAnySkitterIds(t *testing.T) {
  table := NewTableArrayAny()
  TableImportDiskIdsOnly(table.TableInsertId, table.TableEstablishEdge, "./data_skitter")
  Assert(t, table.TableExistsEdge(1, 471), "Edge should exist")
}

func TestTableArrayAnySkitterNeighbours(t *testing.T) {
  table := NewTableArrayAny()
  TableImportDiskIdsOnly(table.TableInsertId, table.TableEstablishEdge, "./data_skitter")
  n, _ := table.TableNeighbours(0)
  Assert(t, n == 360, "Edge should exist")
}

func TestTableArrayAnySkitterSearch(t *testing.T) {
  table := NewTableArrayAny()
  TableImportDiskIdsOnly(table.TableInsertId, table.TableEstablishEdge, "./data_skitter")
  depth, _ := table.TableSearch(471)
  Assert(t, depth > 0, fmt.Sprintf("Depth is %d\n", depth))
}
