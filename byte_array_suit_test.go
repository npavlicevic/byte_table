package table

import (
  "testing"
  "math/rand"
  "time"
  "fmt"
)

func init() {
  rand.Seed(time.Now().UTC().UnixNano())
}

func TestByteArrayExistsCompact(t *testing.T) {
  // todo exists in parallel
  array := NewByteArray()
  n := uint32(0)
  for i := uint32(0); i < size_65k; i += 1 {
    n = rand.Uint32()
    array.ByteArrayAppendCompact(n)
  }
  Assert(t, array.ByteArrayExistsCompact(n), fmt.Sprintf("%d should be in array", n))
}

func TestByteArrayExistsAllCompact(t *testing.T) {
  bytes := []uint32{}
  array := NewByteArray()
  for i := uint32(0); i < size_512b; i += 1 {
    n := rand.Uint32()
    bytes = append(bytes, n)
    array.ByteArrayAppendCompact(n)
  }
  found := array.ByteArrayIterateAllCompact(bytes)
  Assert(t, found, "Should be there")
}

func TestByteArrayFirst(t *testing.T) {
  array := NewByteArray()
  n := rand.Uint32()
  array.ByteArrayAppendCompact(n)
  Assert(t, n == array.ByteArrayFirstCompact(), fmt.Sprintf("%d should be first", n))
}

func TestByteArrayPop(t *testing.T) {
  array := NewByteArray()
  n := rand.Uint32()
  array.ByteArrayAppendCompact(n)
  Assert(t, array.ByteArraySize() > 0, "Should be more than 0")
  array.ByteArrayPopCompact()
  Assert(t, array.ByteArraySize() == 0, "Should be 0")
}
