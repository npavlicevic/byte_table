package table

import(
  "testing"
  "math/rand"
)

func TestQueue(t *testing.T) {
  queue := NewQueue()
  n := rand.Uint32()
  entry := NewEntry()
  entry.id = n
  queue.QueuePush(entry)
  for i := uint32(0); i < size_65k; i += 1 {
    entry = NewEntry()
    entry.id = rand.Uint32()
    queue.QueuePush(entry)
  }
  Assert(t, n == queue.QueueGet(), "Should be first in queue")
}
