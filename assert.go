package table

import (
	"crypto/rand"
	"testing"
)

func Assert(t *testing.T, condition bool, message string) {
	if !condition {
		t.Fatal(message)
	}
}

func AssertError(t *testing.T, condition bool, message string) {
	if !condition {
		t.Error(message)
	}
}

func RandomString(bytes []byte) {
	chars := "abcdefghijklmnopqrstuvwxyz0123456789"
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = chars[b%byte(len(chars))]
	}
}
